import Vue from 'vue';
import Component from 'vue-class-component';
import { Watch } from 'vue-property-decorator';

import './star-rating.scss';

@Component({
    template: require('./star-rating.html'),
    props: {
        rating: Number,
    }
})
export class StarRating extends Vue {

    // Props
    rating;

    // Data
    RATING_MAX_VALUE: number = 5;
    inRating: number = 0;

    mounted () {
        this.inRating = this.rating;
    }

    // Watch method for Reactivity
    @Watch('rating')
    onRatingChanged(val: number, oldVal: number) {
        this.inRating = val;
    }

    // Count Stars
    get stars () {
        return Math.trunc(this.inRating);
    }

    // Count half Stars
    get halfStars () {
        if (this._fractionalPart !== 0) {
            return true;
        } else {
            return false;
        }
    }

    // Count empty Stars
    get emptyStars () {
        if (this._fractionalPart !== 0) {
            return this.RATING_MAX_VALUE - this.stars - 1;
        } else {
            return this.RATING_MAX_VALUE - this.stars;
        }
    }

    // Count fractional part of number
    get _fractionalPart () {
        return this.inRating - this.stars;
    }
}
