import { expect } from 'chai';
import { StarRating } from './StarRating';
import { ComponentTest } from '../../../test/util/ComponentTest';

describe('StarRating component tests', () => {

  let directiveTest: ComponentTest;

  beforeEach(() => {
    directiveTest = new ComponentTest(
      '<div><star-rating :rating="3.56"></star-rating></div>',
      { 'star-rating': StarRating }
    );
  });

  it('Show correct count of full stars', async () => {
    directiveTest.createComponent();
    await directiveTest.execute((vm) => {
      debugger;
      expect(vm.$el.querySelectorAll('.star-rating i.fa-star').length).to.equal(3);
    });
  });

  it('Show correct count of half stars', async () => {
    directiveTest.createComponent();
    await directiveTest.execute((vm) => {
      debugger;
      expect(vm.$el.querySelectorAll('.star-rating i.fa-star-half-o').length).to.equal(1);
    });
  });

  it('Show correct count of empty stars', async () => {
    directiveTest.createComponent();
    await directiveTest.execute((vm) => {
      debugger;
      expect(vm.$el.querySelectorAll('.star-rating i.fa-star-o').length).to.equal(1);
    });
  });

});
