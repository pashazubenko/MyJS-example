import { injectable } from 'inversify';
import moment from 'moment';
import Container from '../../container/IocContainer';
import SERVICES from '../../services/services';
import TranslationProviderInterface from '../../interfaces/TranslationProviderInterface';
import Format from '../../interfaces/Format';

@injectable()
class FormatProvider implements Format {

    protected translationProvider = Container.get<TranslationProviderInterface>(SERVICES.TRANSLATION);
    locale;

    constructor () {
        this.locale = this.translationProvider.getCurentLocale();
    }

    // 1 January 2018
    dateMonthYear(timestamp, locale) {
        moment.locale(locale);
        return moment.unix(timestamp).format('DD MMMM YYYY');
    }

    // January 1st 2018
    monthDateYear(timestamp, locale) {
        moment.locale(locale);
        return moment.unix(timestamp).format('MMMM Do YYYY');
    }

    // 20:15
    time(timestamp) {
        return moment.unix(timestamp).format('HH:mm');
    }

    // 22/9/2019
    fullDate(timestamp) {
        return moment.unix(timestamp).format('DD/MM/YYYY');
    }

    // 1st
    date(timestamp, locale) {
        moment.locale(locale);
        return moment.unix(timestamp).format('Do');
    }
}

export default FormatProvider;
