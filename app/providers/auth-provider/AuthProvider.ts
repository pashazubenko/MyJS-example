import Auth0Lock    from 'auth0-lock';
import JwtDecode    from 'jwt-decode';

// IoC
import { injectable } from 'inversify';
import Auth           from '../../interfaces/Auth';
import Cookie         from '../../interfaces/Cookie';
import Container      from '../../container/IocContainer';
import SERVICES       from '../../services/services';

// Store
import vueStore from '../../store/Store';

@injectable()
class AuthProvider implements Auth {

  auth0: Auth0Lock;

  constructor () {

    // Set Locale for Auth0 provider
    let cookie = Container.get<Cookie>(SERVICES.COOKIE);
    let locale = process.env.DEFAULT_LOCALE;
    if (cookie.get('_icl_current_language')) {
      locale = cookie.get('_icl_current_language');
    }

    this.auth0 = new Auth0Lock(
      process.env.AUTH_CLIENT_ID,
      process.env.AUTH_DOMAIN,
      {
        language: locale,
        scope: 'id_token',
        allowedConnections: [
          'Username-Password-Authentication',
          'google-oauth2',
          'facebook'
        ],
        rememberLastLogin: false,
        loginAfterSignUp: false,
        socialButtonStyle: 'big',
        avatar: null,
        languageDictionary: {
          title: '',
          forgotPasswordTitle: '',
          success: {
            signUp: 'Ga naar je mailbox om je e-mail adres te verifiëren en je bent klaar!'
          }
        },
        theme: {
          'logo': process.env.PATH_HOME + process.env.PATH_BASE + 'img/logo.png',
          'primaryColor': '#00ab3d'
        },
        auth: {
          redirect: false,
          responseType: 'token'
        },
        additionalSignUpFields: [
          {
            name: 'first_name',
            placeholder: 'First name',
            icon: process.env.PATH_HOME + process.env.PATH_BASE + 'img/icons/user-circle.png',
            validator: function(first_name) {
              return {
                valid: first_name.trim().length > 0,
                hint: 'Can`t be blank' // optional
              };
            }
          },
          {
            name: 'last_name',
            placeholder: 'Last name',
            icon: process.env.PATH_HOME + process.env.PATH_BASE + 'img/icons/user-circle.png',
            validator: function(last_name) {
              return {
                valid: last_name.trim().length > 0,
                hint: 'Can`t be blank' // optional
              };
            }
          },
          {
            name: 'lang',
            placeholder: 'language',
            prefill: locale
          },
          {
            name: 'phone_number',
            placeholder: 'Phone number',
            icon: process.env.PATH_HOME + process.env.PATH_BASE + 'img/icons/mobile-phone.png',
            validator: function(phonenumber) {
              return {
                valid: true
              };
            }
          }
        ]
      }
    );

    this.setHendlers();
  }

  login () {
    this.auth0.show();
  }

  setHendlers () {
    let vm = this;

    // On authenticated event hendler
    let authenticated = function(authResult) {
      console.log(authResult);

      let tokenDecoded = JwtDecode(authResult.idToken);
      localStorage.setItem('token_expire', tokenDecoded.exp);

      localStorage.setItem('access_token', authResult.accessToken);
      localStorage.setItem('id_token', authResult.idToken);
      vueStore.dispatch('setAuth');
      vueStore.dispatch('setUser');
      vm.auth0.hide();
    };
    authenticated.bind(vm);
    this.auth0.on('authenticated', authenticated);

    // On hide event hendler
    let hide = function() {
      // Check for 'just_signup' flag in Local Storage
      if (localStorage.getItem('just_signup') === 'true') {
        localStorage.removeItem('just_signup');
        window.location.reload();
      }
      else {
        // Check for Authentication status and redirect to a home
        if (!vm.isAuthenticated()) {
          window.location.href = process.env.PATH_HOME;
        }
      }
    };
    hide.bind(vm);
    this.auth0.on('hide', hide);

    // Authorization Error lambda function
    this.auth0.on('authorization_error', function (msg) {
      console.error(msg);
    });

    // Signup Button hit set Flag to Local Storage
    this.auth0.on('signup submit', function () {
      localStorage.setItem('just_signup', 'true');
    });
  }

  token() {
    return localStorage.getItem('id_token');
  }

  logout () {
    localStorage.removeItem('access_token');
    localStorage.removeItem('token_expire');
    localStorage.removeItem('id_token');
    vueStore.dispatch('setAuth', false);
    vueStore.dispatch('setUser', {});
    window.location.href = process.env.PATH_HOME;
  }

  isAuthenticated () {
    if (localStorage.getItem('id_token') && localStorage.getItem('token_expire')) {
      let token_expire = + localStorage.getItem('token_expire') * 1000;
      let now = new Date().getTime();
      if (token_expire > now) {
        return true;
      }
    }
    else {
      return false;
    }
  }

  authPromise() {
    return Promise.resolve(this.login());
  }
}

export default AuthProvider;
