interface UserInterface {
    id;
    first_name;
    last_name;
    email;
    phone;
    membership;
}
export default UserInterface;
