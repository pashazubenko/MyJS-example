interface CustomerApiProviderInterface {
  getBookingsHistory();
  getBookingsHistoryPage(next_page);
  getUserInfo();
  setUserInfo(user);
  getUserSettings();
  getCountries();
  getUserReviews();
}

export default CustomerApiProviderInterface;
