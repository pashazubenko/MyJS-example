const SERVICES = {
    COOKIE      : Symbol('Cookie'),
    AUTH        : Symbol('Auth'),
    CUSTOMER    : Symbol('CustomerApiProviderInterface'),
    TRANSLATION : Symbol('TranslationProviderInterface'),
    MENUS       : Symbol('MenuProviderInterface'),
    FORMAT      : Symbol('Format')
};

export default SERVICES;
