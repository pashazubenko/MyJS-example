import Vue from 'vue';

import VueI18n from 'vue-i18n';
import nl from './nl';

Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: process.env.DEFAULT_LOCALE,
  silentTranslationWarn: true,
  messages: {
    'nl': nl,
    'en': {},
    'de': {},
    'es': {}
  }
});

export default i18n;
