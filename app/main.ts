import Vue from 'vue';

// Styles
import './resources/sass/main.scss';

// Router
import router from './router/Router';

// Vuex Store
import store from './store/Store';

// Locales
import i18n from './locales/i18n';

// Run base App component
import App from './components/base/app/App';

export const app = new Vue({
  el: '#app-main',
  i18n: i18n,
  store: store,
  router: router,
  render: h => h(App)
});

// Translation filter
Vue.filter('t', function (value, arg = null) {
  return app.$i18n.t(value, app.$store.getters.locale);
});
