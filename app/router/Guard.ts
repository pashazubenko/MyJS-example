import Auth      from '../interfaces/Auth';
import IGuard    from './../interfaces/Guard';

export default {

    guest (to, from, next) {
        next();
    },

    auth (to, from, next) {
        if (localStorage.getItem('token')) {
            next();
        } else {
            console.error('You have no Token');
        }
    }
};

