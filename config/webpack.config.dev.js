const helpers = require('./helpers'),
  env = require('../environment/dev.env'),
  webpackConfig = require('./webpack.config.base'),
  DefinePlugin = require('webpack/lib/DefinePlugin'),
  SourceMapDevToolPlugin = require('webpack/lib/SourceMapDevToolPlugin'),
  HtmlWebpackPlugin = require('html-webpack-plugin'),
  ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractSass = new ExtractTextPlugin({
  filename: 'css/[name].[contenthash].css'
});

webpackConfig.module.rules = [...webpackConfig.module.rules,
  {
    test: /\.css$/,
    use: [
      'vue-style-loader',
      'css-loader'
    ],
  },
  {
    test: /\.scss$/,
    use: extractSass.extract({
      use: [{
          loader: 'css-loader',
          options: {
            minimize: false,
            sourceMap: true,
            importLoaders: 2
          }
        },
        {
          loader: 'sass-loader',
          options: {
            outputStyle: 'expanded',
            sourceMap: true,
            sourceMapContents: true
          }
        }
      ],
      fallback: 'style-loader'
    })
  },
  {
    test: /\.(jpg|png|gif)$/,
    loader: 'file-loader?name=/img/[name].[hash].[ext]'
  },
  {
    test: /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
    use: [{
      loader: 'file-loader',
      options: {
        name: '[name].[ext]',
        outputPath: 'fonts/', // where the fonts will go
        publicPath: '../' // override the default path
      }
    }]
  }
];

webpackConfig.plugins = [...webpackConfig.plugins, extractSass,

  new SourceMapDevToolPlugin({
    filename: null, // if no value is provided the sourcemap is inlined
    test: /\.(ts|js)($|\?)/i
  }),

  new HtmlWebpackPlugin({
    inject: true,
    template: helpers.root('/app/index.html'),
  }),

  new DefinePlugin({
    'process.env': env
  })
];

module.exports = webpackConfig;
